@extends('layout')

@section('content')
    <section id="beranda" data-anchor="beranda">
		<div class="slider-banner">
			<div class="item">
				<div class="bg-banner">
					<div class="tbl">
						<div class="cell">
							<div class="container">
								<div class="row">
									<div class="col-md-6 col-lg-5">
										<div class="t animated">Solusi Terpercaya untuk Pemasaran Bisnis Anda!</div>
										<div class="bdy animated">
											<p class="mb8">Nilai brand adalah komponen utama dari bisnis Anda untuk menggambarkan seberapa kuat dan “mengena” brand tersebut pada benak customer Anda. Tujuan dari nilai brand sendiri adalah untuk menciptakan loyality pada customer yang telah Anda punya.</p>
											<p class="mb8">Tingkatkan segera nilai brand Anda dengan <span class="blue">visualisasi yang menarik</span> dan <span class="blue">tepat sasaran</span> agar customer tetap setia menjaga brand Anda pada top of mind mereka bersama LakuCreative.</p>
											<p>Anda akan mendapatkan layanan <span class="blue italic">design marketing, branding consulting, branding concepting</span> serta <span class="blue italic">content creating</span> dari profesional spesial yang hanya ada di LakuCreative.</p>
										</div>
										<div class="text-center animated">
											<button class="link-btn click-hubungi">siap pesan</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="abs-img"><img src="{{asset('images/banner1.png?v.1')}}" title="" alt=""/></div>
				</div>
			</div>
			<div class="item">
				<div class="bg-banner">
					<div class="tbl">
						<div class="cell">
							<div class="container">
								<div class="row">
									<div class="col-md-6 my-auto">
										<div class="img">
											<img src="{{asset('images/banner2.png?v.1')}}" title="" alt=""/>
										</div>
									</div>
									<div class="col-md-6 col-lg-5 my-auto offset-lg-1">
										<div class="t text-right animated">
											<div>Nilai Brand Anda adalah Segalanya!</div>
										</div>
										<div class="bdy animated text-right">
											<p class="mb8">Bayangkan apabila Anda adalah pemilik sebuah bisnis kosmetik / skincare yang tidak memiliki brand ataupun logo pada kemasan-nya. Ketika ada customer yang ingin membelinya, apakah mereka dapat menemukan produk Anda dan dapat membeli kembali ?</p>
											<p class="mb8">Apabila produk Anda sangat dibutuhkan oleh customer, namun customer tidak dapat menemukannya karena produk Anda tidak memiliki nama ataupun logo yang menjadi dasar dari identitas produk Anda, maka Anda akan kehilangan <span class="blue italic">potential sales!</span></p>
											<p class="mb8">LakuCreative dapat membantu Anda meningkatkan nilai brand dari bisnis yang Anda miliki dengan tepat dan menarik customer sehingga pasar Anda tetap terjaga</p>
										</div>
										<div class="text-center animated">
											<button class="link-btn click-hubungi">siap pesan</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="abs-img"><img src="{{asset('images/banner3.png?v.1')}}" title="" alt=""/></div>
				</div>
			</div>
		</div>
		<div class="bg" id="siap" style="background: url('{{asset('images/banner-siap.png?v.1')}}') no-repeat bottom;">
			<div class="container">
				<div class="t sr-up-td1">Lakucreative siap membantu Anda!</div>
				<div class="bdy sr-up-td2">7 Alasan Anda dapat Percaya LakuCreative Yaitu...</div>
				<div class="row">
					<div class="col-md-6 col-lg-5">
						<div class="bdr sr-left-td3">Brand Anda Akan ditangani oleh profesional Brand Consultant berpengalaman</div>
						<div class="bdr sr-left-td4">Brand Anda Akan ditangani oleh profesional Marketing Designer berpengalaman</div>
						<div class="bdr sr-left-td5">Brand Anda Akan ditangani oleh profesional Content Creator berpengalaman</div>
						<div class="bdr sr-left-td6">Implementasi konsep sesuai dengan keinginan dan kebutuhan Anda</div>
					</div>
					<div class="col-md-6 col-lg-5 offset-lg-1">
						<div class="bdr sr-left-td7">Pengerjaan dan penyelesaian pesanan sesuai dengan waktu yang ditentukan</div>
						<div class="bdr sr-left-td8">Harga yang ditawarkan tidak akan memberatkan keinginan dan kebutuhan Anda.</div>
						<div class="bdr sr-left-td9">Konsultasi yang sangat mudah hanya Chat melalui Whatsapp saja</div>
						<div class="text-center sr-left-td10">
							<button class="link-btn click-hubungi">siap pesan</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="tentang-kami" data-anchor="tentang-kami">
		<div class="bg" id="tentang">
			<div class="container">
				<div class="t sr-up-td1">
					<div>Kenapa sih perlu</div>
					<div>Lakucreative?</div>
				</div>
				<div class="row">
					<div class="col-md-10 offset-md-1">
						<Div class="bdy sr-up-td2">
							<p>Jaman sekarang, konsumen udah pada pinter, apa-apa promosi sana sini, capek tawarin produk tapi tidak laku-laku. Bener gak? Untuk para usahawan, penting banget nih sekarang brandnya mudah <span class="blue">dikenali dan diingat</span>, bagaimana caranya? Tahap awal untuk memperluas jaringan pemasaran adalah dengan memiliki <span class="blue">brand sendiri</span>.</p> 
							<p>Untuk mengenal lebih dalam apakah Lakucreative itu, yuk klik di bawah ini:</p>
						</Div>
						<div class="text-center sr-up-td3">
							<a href="{{ URL::to('/tentang-kami') }}">
								<button class="link-btn">Tentang Kami</button>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="jasa-desain" data-anchor="jasa-desain">
		<div class="bg" id="desain">
			<div class="container">
				<div class="bdr-desain"></div>
				<div class="t sr-up-td1">
					<div>Semua Desain untuk</div>
					<div>kebutuhan Anda!</div>
				</div>
				<div class="tab-content">
					<div class="tab-pane fade show active" id="sosial" role="tabpanel" aria-labelledby="sosial-tab">
						<div class="row">
							<div class="col-6 col-md-3 my-auto sr-up-td2">
								<div class="img">
									<a href="{{asset('images/big-sosial1.png?v.1')}}" data-fancybox="sosial">
										<img src="{{asset('images/sosial1.jpg?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto sr-up-td3">
								<div class="img">
									<a href="{{asset('images/big-sosial2.png?v.1')}}" data-fancybox="sosial">
										<img src="{{asset('images/sosial2.jpg?v.1')}}assets/images/sosial2.jpg" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto sr-up-td4">
								<div class="img">
									<a href="{{asset('images/big-sosial3.png?v.1')}}" data-fancybox="sosial">
										<img src="{{asset('images/sosial3.jpg?v.1')}}assets/images/sosial3.jpg" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto sr-up-td5">
								<div class="img">
									<a href="{{asset('images/big-sosial4.png?v.1')}}" data-fancybox="sosial">
										<img src="{{asset('images/sosial4.jpg?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="kemasan" role="tabpanel" aria-labelledby="kemasan-tab">
						<div class="row">
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-kemasan1.png?v.1')}}" data-fancybox="kemasan">
										<img src="{{asset('images/kemasan1.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-kemasan2.png?v.1')}}" data-fancybox="kemasan">
										<img src="{{asset('images/kemasan2.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-kemasan3.png?v.1')}}" data-fancybox="kemasan">
										<img src="{{asset('images/kemasan3.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-kemasan4.png?v.1')}}" data-fancybox="kemasan">
										<img src="{{asset('images/kemasan4.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="marketing" role="tabpanel" aria-labelledby="marketing-tab">
						<div class="row">
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-marketing1.png?v.1')}}" data-fancybox="marketing">
										<img src="{{asset('images/marketing1.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img small">
									<a href="{{asset('images/marketing2.png?v.1')}}" data-fancybox="marketing">
										<img src="{{asset('images/marketing2.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-marketing3.png?v.1')}}" data-fancybox="marketing">
										<img src="{{asset('images/marketing3.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/marketing4.png?v.1')}}" data-fancybox="marketing">
										<img src="{{asset('images/marketing4.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane fade" id="stationery" role="tabpanel" aria-labelledby="stationery-tab">
						<div class="row">
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-stationery1.png?v.1')}}" data-fancybox="stationery">
										<img src="{{asset('images/stationery1.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-stationery2.png?v.1')}}" data-fancybox="stationery">
										<img src="{{asset('images/stationery2.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-stationery3.png?v.1')}}" data-fancybox="stationery">
										<img src="{{asset('images/stationery3.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
							<div class="col-6 col-md-3 my-auto">
								<div class="img">
									<a href="{{asset('images/big-stationery4.png?v.1')}}" data-fancybox="stationery">
										<img src="{{asset('images/stationery4.png?v.1')}}" title="" alt=""/>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<ul class="nav nav-pills" role="tablist">
					<li class="nav-item sr-up-td2">
						</a>
						<a class="active" id="sosial-tab" data-toggle="pill" href="#sosial" role="tab" aria-controls="sosial" aria-selected="true">Sosial Media</a>
						<ul class="l-desain">
							<li>Feed Instagram</li>
							<li>Feed Facebook</li>
							<li>Banner Website</li>
							<li>Banner Marketplace</li>
						</ul>
					</li>
					<li class="nav-item sr-up-td3">
						<a class="nav-link" id="kemasan-tab" data-toggle="pill" href="#kemasan" role="tab" aria-controls="kemasan" aria-selected="false">Kemasan</a>
						<ul class="l-desain">
							<li>Packaging</li>
							<li>Label</li>
						</ul>
					</li>
					<li class="nav-item sr-up-td4">
						<a class="nav-link" id="marketing-tab" data-toggle="pill" href="#marketing" role="tab" aria-controls="marketing" aria-selected="false">Marketing Kit</a>
						<ul class="l-desain">
							<li>Banner</li>
							<li>Brosur</li>
							<li>Flyer</li>
							<li>Poster</li>
						</ul>
					</li>
					<li class="nav-item sr-up-td5">
						<a class="nav-link" id="stationery-tab" data-toggle="pill" href="#stationery" role="tab" aria-controls="stationery" aria-selected="false">Stationery</a>
						<ul class="l-desain">
							<li>Kartu Nama</li>
							<li>Kop Surat</li>
							<li>Nota</li>
							<li>Dll</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</section>


	<section id="cara-pemesanan" data-anchor="cara-pemesanan">
		<div class="bg getMenu" id="cara" style="background: url('{{asset('images/banner-carakerja.jpg?v.1')}}') no-repeat center;">
			<div class="container">
				<div class="t item sr-up-td1">cara kerja</div>
				<div class="t2 item sr-up-td2">
					<div>Langkah Mudah Pemesanan</div>
					<div>Desain Lakucreative</div>
				</div>
				<div class="row">
					<div class="col-20 item sr-up-td3">
						<div class="item-cell">
							<div class="img"><img src="{{asset('images/kerja1.png?v.1')}}" alt="" title=""/></div>
						</div>
						<div class="item-cell pl15">
							<div class="nm">Konsultasi</div>
							<div class="text">Melalui Whatsapp dengan Konsultan brand yang berpengalaman menangani berbagai macam brand dan produk</div>
						</div>
					</div>
					<div class="col-20 item sr-up-td4">
						<div class="item-cell">
							<div class="img"><img src="{{asset('images/kerja2.png?v.1')}}" alt="" title=""/></div>
						</div>
						<div class="item-cell pl15">
							<div class="nm">konfirmasi</div>
							<div class="text">Order Anda dan isi form order yang kami berikan</div>
						</div>
					</div>
					<div class="col-20 item sr-up-td5">
						<div class="item-cell">
							<div class="img"><img src="{{asset('images/kerja3.png?v.1')}}" alt="" title=""/></div>
						</div>
						<div class="item-cell pl15">
							<div class="nm">bayar</div>
							<div class="text">Pesanan Anda disesuaikan dengan totalan kami agar pesanan Anda segera kami proses</div>
						</div>
					</div>
					<div class="col-20 item sr-up-td6">
						<div class="item-cell">
							<div class="img"><img src="{{asset('images/kerja4.png?v.1')}}" alt="" title=""/></div>
						</div>
						<div class="item-cell pl15">
							<div class="nm">draft</div>
							<div class="text">Akan kami kirimkan untuk Anda periksa dan daftar revisi yang diperlukan sesuai dengan ketentuan dari kami</div>
						</div>
					</div>
					<div class="col-20 item sr-up-td7">
						<div class="item-cell">
							<div class="img"><img src="{{asset('images/kerja5.png?v.1')}}" alt="" title=""/></div>
						</div>
						<div class="item-cell pl15">
							<div class="nm">file</div>
							<div class="text">yang sudah final akan dikirim melalui email / link G-drive dan siap untuk digunakan</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="testimonial" data-anchor="testimonial">
		<div class="bg getMenu" id="testimoni">
			<div class="abs-testi">
				<img src="{{asset('images/banner-testimoni.png?v.1')}}" title="" alt=""/>
			</div>
			<div class="abs-testi-left">
				<img src="{{asset('images/testi1.png?v.1')}}" title="" alt=""/>
			</div>
			<div class="abs-testi-right">
				<img src="{{asset('images/testi2.png?v.1')}}" title="" alt=""/>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-lg-7 sr-left-td1">
						<div class="img"><img src="{{asset('images/testimoni.png?v.1')}}" alt="" title=""/></div>
					</div>
					<div class="col-md-6 col-lg-5 sr-right-td1">
						<div class="pl50">
							<div class="t">Mengapa memilih Lakucreative?</div>
							<div class="bdy">
								<div>Sudah, Jasa Marketing Memang Berat,</div>
								<div><span class="blue">Kamu</span> Engga Akan Kuat.</div>
								<div>Biar <span class="blue">LakuCreative</span> Aja~</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="bg-testi">
				<div class="container">
					<div class="t2">Testimoni</div>
					<div class="slider-testimoni">
						<div class="item">
							<div class="bdy-testi">
								<p>“ Hasil Desainnya bagus dan cepat banget jadinya. gak pake revisi langsung klop. very recommended ”</p>
							</div>
							<div class="nm-testi">Nicky Ruliana Sari, @Blessglow.official</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Thanks yaa, order buat 15 gambar feed, adminnya sabar banget,, untung nya fast respond,, jadi komunikasinya baik, thanks yaa! hasilnya puas! ”</p>
							</div>
							<div class="nm-testi">Dwi Audit, @sada.kids</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Super puas, lucuuuu, fast respon, cepet jadinya dan yang pasti enak banget diajak konsultasi meskipun akunya agak rewel tapi bener-bener diladenin dengan sabar. Love bgt! ”</p>
							</div>
							<div class="nm-testi">Wanda Resita, @miware.id</div>
						</div>

						<div class="item">
							<div class="bdy-testi">
								<p>“ Terima kasih lakucreative, email projek madina sudah saya terima. Hasilnya bagus bgt. Selalu memuaskan. Sukses selalu lakucreative ”</p>
							</div>
							<div class="nm-testi">Addina, @madina.babykids</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Desain disini hasilnya bagus banget, ngerti apa yang aku mau gausah pake revisi langsung klop. Maaci lakucreative! ”</p>
							</div>
							<div class="nm-testi">Meilia Putri Zaida, @laksanadaster</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Sangat memuaskan, pengerjaan rapi dan cepat, admin sangat mengerti kebutuhan yang di inginkan untuk hasil terbaik ”</p>
							</div>
							<div class="nm-testi">Afdhal Firlando, @zubair.co.id</div>
						</div>

						<div class="item">
							<div class="bdy-testi">
								<p>“ Pelayanan memuaskan. Hasil design juga bagus. Harga terjangkau ”</p>
							</div>
							<div class="nm-testi">Renny Apriani, @oilsome.id</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Respon admin nya enak banget, ga nyesel deh Order Disini ”</p>
							</div>
							<div class="nm-testi">Aji Setyawan, @j.mask.id</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Jasa desain terbaik, yang bisa merubah konsep jadi postingan ”</p>
							</div>
							<div class="nm-testi">Ryan Disastra, @organicmart.id</div>
						</div>

						<div class="item">
							<div class="bdy-testi">
								<p>“ Hasil design cepat dn memuaskan bgt, Uda gt admin nya jg baik dan cpt mengerti apa yg di mau, sampe ga bsa kelain hati haha ”</p>
							</div>
							<div class="nm-testi">Rusila Puty, @dwilbeuty</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Desain bagus, sesuai yg diinginkan, komunikasi oke. Sukses yaa! ”</p>
							</div>
							<div class="nm-testi">Editha Meydiana, @pixeldental.id</div>
						</div>
						<div class="item">
							<div class="bdy-testi">
								<p>“ Hasil kerjanya sangat bagus sama tepat waktuu ”</p>
							</div>
							<div class="nm-testi">Ryan Disastra, @organicmart.id</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section id="hubungi-kami" data-anchor="hubungi-kami">
		<div class="bg getMenu" id="hubungi">
			<div class="container">
				<div class="t">
					<div>Siap bangun brand Anda dengan</div>
					<div>Lakucreative? Hubungi kami!</div>
				</div>
				<div class="row">
					<div class="col-6 col-md-3 offset-md-2 product-animated-left">
						<div class="item">
							<div class="img"><img src="{{asset('images/hubungi1.png?v.1')}}" alt="" title=""/></div>
							<a href="https://forms.gle/2sZoFEgKMgGWyKQM7	" target="_blank"> 
								<img src="{{asset('images/btn1.png?v.1')}}" alt="" title=""/>
							</a>
						</div>
					</div>
					<div class="col-6 col-md-3 offset-md-2 product-animated-right">
						<div class="item">
							<div class="img"><img src="{{asset('images/hubungi2.png?v.1')}}" alt="" title=""/></div>
							<a href="https://Bit.ly/LakuCreative" target="_blank"> 
								<img src="{{asset('images/btn2.png?v.1')}}" alt="" title=""/>
							</a>
						</div>
					</div>
				</div>
				<div class="metode product-animated-top">
					<div class="t2">Metode Pembayaran</div>
					<ul class="l">
						<li><img src="{{asset('images/bca.png?v.1')}}" alt="" title=""/></li>
						<li><img src="{{asset('images/mandiri.png?v.1')}}" alt="" title=""/></li>
					</ul>
				</div>	
			</div>
		</div>
	</section>
    
@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.slider-banner').on('init', function(event, slick){
            $('.animated').addClass('activate fadeInUp');
        });

        $('.slider-banner').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            infinite: true,
            autoplay: true,
            arrows: false,
            autoplaySpeed: 3500,
            responsive: [
	        {
	            breakpoint: 768,
	            settings: {
	                adaptiveHeight: true
	            }
	        }
	        ]
        });

        $('.slider-banner').on('afterChange', function(event, slick, currentSlide) {
            $('.animated').removeClass('off');
            $('.animated').addClass('activate fadeInUp');
        });

        $('.slider-banner').on('beforeChange', function(event, slick, currentSlide) {
            $('.animated').removeClass('activate fadeInUp');
            $('.animated').addClass('off');
        });

		$('.slider-testimoni').slick({
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        dots: false,
	        infinite: true,
	        autoplay: true,
	        centerMode: false,
	        autoplaySpeed: 2000,
	        arrows: true,
	        responsive: [
	        {
	            breakpoint: 768,
	            settings: {
	                slidesToShow: 1,
	                slidesToScroll: 1,
	                adaptiveHeight: true
	            }
	        }
	        ]
	    });

	    $("[data-fancybox]").fancybox({
			infobar : false,
			buttons : [
				'close'
			],
			loop: true,
		});
	});	
</script>
@endsection