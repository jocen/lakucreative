@extends('layout')

@section('content')
    <div class="pad-tentang">
		<div class="container">
			<div class="pad50">
				<div class="row">
					<div class="col-md-6 sr-left-td1">
						<div class="img"><img src="{{asset('images/tentang1.png?v.1')}}" title="" alt=""/></div>
					</div>
					<div class="col-md-6 sr-right-td1">
						<div class="t">Lakucreative adalah...</div>
						<div class="bdy">
							<p class="mb10">Layanan multijasa yang menyediakan <span class="blue">layanan design marketing, branding consulting, branding concepting,</span> serta <span class="blue">content</span> creating untuk menarik daya beli customer Anda sesuai dengan desire dan interest mereka serta menguatkan potensi dari nilai brand yang Anda miliki untuk mendapatkan posisi top of mind dari pasar bisnis Anda.</p>
							<p>LakuCreative memiliki visi dan misi untuk membantu perusahaan serta UKM di Indonesia seperti Anda agar nilai brand lebih dikenal oleh para customer dan mendapatkan customer loyalty tertinggi yaitu <span class="blue">Level Advocate</span>. Kami bertujuan untuk menarik para customer Anda agar dapat menjadi sebuah kelompok customer advocate yang akan berjuang bersama perusahaan Anda.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="pad50">
				<div class="row">
					<div class="col-md-6 order-2 order-md-1 right sr-left-td2">
						<div class="t">Customer Advocate adalah...</div>
						<div class="bdy">
							<p>Customer setia <span class="blue">( loyal customer )</span> yang fanatik menggunakan produk/ jasa perusahaan Anda dan mereka mau berbagi cerita baik tentang perusahaan Anda. Kelompok ini juga secara spontan akan membela perusahaan Anda karena inisiatif / keinginannya sendiri. Semakin banyak customer advocate yang tercipta, maka perusahaan Anda akan memenangkan persaingan dan menjadi juara di hati customer.</p>
						</div>
					</div>
					<div class="col-md-6 order-1 order-md-2 sr-right-td2">
						<div class="img"><img src="{{asset('images/tentang2.png?v.1')}}" title="" alt=""/></div>
					</div>
				</div>
			</div>
			<div class="pad50">
				<div class="row">
					<div class="col-md-6 sr-left-td1">
						<div class="img"><img src="{{asset('images/tentang3.png?v.1')}}" title="" alt=""/></div>
					</div>
					<div class="col-md-6 sr-right-td1">
						<div class="t">Pentingnya Nilai Brand Kamu bagi Lakucreative itu...</div>
						<div class="bdy">
							<p>LakuCreative sangat mengerti bahwa brand Anda pasti memiliki keunikan dan potensi tersendiri. Makna, cita – cita, atau asa yang diciptakan oleh sang pemiliki brand, kami jadikan sebuah nilai tambah yang perlu untuk terus diperbarui dan dikenali oleh para customer dari brand Anda. Dengan menonjolkan nilai tambah tersebut, maka loyalitas dari customer akan dapat tercapai. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
    
@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav_home').addClass('active');
	});	
</script>
@endsection